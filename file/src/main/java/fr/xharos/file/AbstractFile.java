package fr.xharos.file;

import java.io.*;

/**
 * File <b>AbstractFile</b> located on fr.xharos.file
 * AbstractFile is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 17/07/2017 at 16:36
 * @since 0.0.1
 */
public abstract class AbstractFile {

    protected final File file;

    public AbstractFile(String fileName) {
        this(new File(fileName));
    }

    public AbstractFile(File file) {
        if (file == null)
            throw new UnsupportedOperationException("Given File cannot be null");
        this.file = file;
    }

    public boolean exist() {
        return file.exists();
    }

    public boolean create() {
        if (!isDirectory())
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        else
            return file.mkdir();
    }

    public String name() {
        return file.getName();
    }

    public String path() {
        return file.getPath().replace('\\', '/');
    }

    public String extension() {
        String name = name();
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex == -1)
            return "";
        return name.substring(dotIndex + 1);
    }

    public String nameWithoutExtension() {
        String name = file.getName();
        int dotIndex = name.lastIndexOf('.');
        if (dotIndex == -1)
            return name;
        return name.substring(0, dotIndex);
    }

    public boolean isDirectory() {
        return file.isDirectory();
    }

    public abstract void load() throws IOException;

    public abstract void save() throws IOException;

    public InputStream read() {
        try {
            return new FileInputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public OutputStream write() {
        try {
            return new FileOutputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Writer writer() {
        return new OutputStreamWriter(write());
    }

    public Reader reader() {
        return new InputStreamReader(read());
    }
}
