package fr.xharos.file.properties;

import fr.xharos.file.AbstractFile;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * File <b>PropertiesFile</b> located on fr.xharos.file.properties
 * PropertiesFile is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 17/07/2017 at 16:45
 * @since 0.0.1
 */
public class PropertiesFile extends AbstractFile {

    private final Properties properties;

    public PropertiesFile(String fileName) {
        this(new File(fileName));
    }

    public PropertiesFile(File file) {
        super(file);
        this.properties = new Properties();
    }

    @Override
    public void load() throws IOException {
        if (exist())
            properties.load(read());
        else
            throw new IOException("File does not exist");
    }

    @Override
    public void save() throws IOException {
        if (exist())
            properties.store(write(), null);
        else
            throw new IOException("File does not exist");
    }

    public void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
