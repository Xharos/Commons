package fr.xharos.pattern;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * File <b>BuilderPattern</b> located on fr.xharos.pattern
 * BuilderPattern is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 01/07/2017 at 23:36
 * @since 0.0.1
 */
public abstract class BuilderPattern<T> {

    protected final Set<Consumer<T>> modifiers = new HashSet<>();

    /**
     * Construct this builder, after perform :
     * <code>
     * modifiers.forEach(consumer -> consumer.accept(T));
     * </code>
     *
     * @return a new instance based on this genericType
     */
    public abstract T build();

    /**
     * Store this consumer on{@link BuilderPattern#modifiers}
     *
     * @param consumer value to consume with the generic handler
     * @return this {@link BuilderPattern} instance
     * @see BuilderPattern#build()
     */
    public BuilderPattern<T> with(Consumer<T> consumer) {
        modifiers.add(consumer);
        return this;
    }
}
