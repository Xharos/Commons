package fr.xharos.reflection;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * File <b>Types</b> located on fr.xharos.reflection
 * Types is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 02/07/2017 at 00:04
 * @since 0.0.1
 */
public class Types {

    private Types() {

    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClazz(Type type) {
        if (type instanceof Class<?>)
            return (Class<T>) type;
        else if (type instanceof ParameterizedType)
            return (Class<T>) ((ParameterizedType) type).getRawType();
        else
            return null;
    }

}
