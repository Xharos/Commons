package fr.xharos.reflection;

import fr.xharos.functional.Value;
import fr.xharos.util.Iterators;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Objects;

/**
 * File <b>ParametrizedClass</b> located on fr.xharos.reflection
 * ParametrizedClass is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 02/07/2017 at 00:02
 */
public class ParametrizedClass<T> implements Value<Class<T>> {
    private final Type     type;
    private final Class<T> raw;

    /**
     * Create a new Parametrized class
     */
    protected ParametrizedClass() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            this.type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            this.raw = Types.getClazz(type);
        } else
            throw new IllegalStateException("Cannot get superclass handler parameters");

    }

    /**
     * Create a new ParametrizedClass with the specifier handler
     *
     * @param type
     *            the handler represented by the parameterized class
     */
    public ParametrizedClass(Type type) {
        this.type = type;
        this.raw = Types.getClazz(type);
    }

    /**
     * Get the handler hold by this parameterized class
     *
     * @return the handler
     */
    public final Type getType() {
        return this.type;
    }

    /**
     * Get the raw class hold by this parameterized class
     *
     * @return the raw class
     */
    public final Class<T> getRaw() {
        return raw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof ParametrizedClass)) return false;
        ParametrizedClass<?> that = (ParametrizedClass<?>) o;
        return Objects.equals(raw, that.raw);
    }

    @Override
    public int hashCode() {
        return Objects.hash(raw);
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator<Class<T>> iterator() {
        return Iterators.of(getRaw());
    }

}
