package fr.xharos.test;

import fr.xharos.reactor.Reactor;
import fr.xharos.reactor.ReactorFactory;
import fr.xharos.reactor.handler.Type;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

/**
 * File <b>ServerTest</b> located on fr.xharos.test
 * ServerTest is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 04/07/2017 at 00:15
 * @since 0.0.3
 */
public class ServerTest {

    public ServerTest(int port) throws Exception {
        Reactor reactor = ReactorFactory.init("localhost", port).initReactor(ReactorFactory.Side.SERVER);
        setHandler(reactor);
        //reactor.setOptions("ByteBufferSize", "1024");
        reactor.setOptions("Debug", "true");
        reactor.launchReactor();
    }

    public static void main(String[] args) throws Exception {
        new ServerTest(11111);
    }

    private void setHandler(Reactor reactor) {
        reactor.setHandler(Type.ACCEPT, (channel) -> {
            try {
                channel.getChannel().configureBlocking(false);
                channel.getChannel().finishConnect();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        });
        reactor.setHandler(Type.DICSONNECT, (channel) -> {
            try {
                channel.getChannel().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false; //doesn't really care here
        });
        reactor.setHandler(Type.READ, (channel) -> {
            ByteBuffer buffer = channel.getReaderBuffer();
            int numRead;
            try {
                numRead = channel.getChannel().read(buffer);

                if (numRead == 0) {
                    return true; //Simply ignore
                }

                if (numRead == -1) {
                    return false; //Return false will kick the Channel from the server!
                }
                if (buffer.limit() >= 1) { //More than 1 byte
                    buffer.flip();
                    while (buffer.remaining() != 0) //Recursive read
                        buffer.get();
                }
                buffer.compact();
                reactor.subscribeBuffers(channel, ByteBuffer.allocate(1).put((byte) new Random().nextInt(127))); //Echo a Random byte
                return true;
            } catch (IOException e) {
                //e.printStackTrace();
                return false;
            }
        });
        reactor.setHandler(Type.WRITE, (channel) -> {
            try {
                for (ByteBuffer data : channel.getBuffers()) {
                    data.flip();
                    int write = channel.getChannel().write(data);
                    while (write > 0 && data.hasRemaining()) {
                        write = channel.getChannel().write(data);
                    }
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        });
    }
}
