package fr.xharos.test;

import fr.xharos.reactor.Reactor;
import fr.xharos.reactor.ReactorFactory;
import fr.xharos.reactor.handler.Type;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * File <b>ClientTest</b> located on fr.xharos.test
 * ClientTest is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 04/07/2017 at 00:22
 * @since 0.0.3
 */
public class ClientTest {

    private Reactor reactor;

    public ClientTest(int port) throws Exception {
        this.reactor = ReactorFactory.init("localhost", port).initReactor(ReactorFactory.Side.CLIENT);
        setHandler(reactor);
        reactor.launchReactor();
    }

    public static void main(String[] args) throws Exception {
        new ClientTest(21547);
    }

    private void setHandler(Reactor reactor) {
        reactor.setHandler(Type.ACCEPT, (channel) -> {
            try {
                channel.getChannel().finishConnect();
                System.out.println("Connect to -> " + channel.getChannel().getRemoteAddress() + "; with id" + channel.getId());
                ByteBuffer buffer = ByteBuffer.allocate(1);
                buffer.put((byte) 0);
                reactor.subscribeBuffers(channel, buffer);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        });
        reactor.setHandler(Type.DICSONNECT, (channel) -> {
            try {
                channel.getChannel().close();
                System.out.println("Client -> " + channel.getChannel().getRemoteAddress() + " / " + channel.getId() + " disconnect!");
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        });
        reactor.setHandler(Type.READ, (channel) -> {
            ByteBuffer buffer = channel.getReaderBuffer();
            int numRead;
            try {
                numRead = channel.getChannel().read(buffer);
            } catch (IOException e) {
                return false;
            }

            if (numRead == 0) {
                return true;
            }

            if (numRead == -1) {
                return false;
            }

            if (buffer.remaining() < 1) // Can't read yet
                return true;

            buffer.flip();
            System.out.println(buffer.get());
            return true;
        });
        reactor.setHandler(Type.WRITE, channel -> {
            try {
                System.out.println("Write to buffer -> " + channel.getChannel().getRemoteAddress() + " / " + channel.getId());
                for (ByteBuffer data : channel.getBuffers()) {
                    data.flip();
                    int write = channel.getChannel().write(data);
                    while (write > 0 && data.hasRemaining()) {
                        write = channel.getChannel().write(data);
                    }
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        });
    }
}
