package fr.xharos.reactor;

import java.net.InetSocketAddress;

/**
 * File <b>ReactorFactory</b> located on fr.xharos.reactor
 * ReactorFactory is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 23:43
 * @since 0.0.2
 */
public class ReactorFactory {

    private final InetSocketAddress address;

    private ReactorFactory(InetSocketAddress address) {
        this.address = address;
    }

    public static ReactorFactory init(String host, int port) {
        return init(new InetSocketAddress(host, port));
    }

    public static ReactorFactory init(InetSocketAddress address) {
        return new ReactorFactory(address);
    }

    public Reactor initReactor(Side side) {
        return Reactor.open(address, side);
    }

    public static enum Side {
        CLIENT,
        SERVER;
    }

}
