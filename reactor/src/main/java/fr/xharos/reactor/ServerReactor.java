package fr.xharos.reactor;

import fr.xharos.reactor.ReactorFactory.Side;
import fr.xharos.reactor.handler.Type;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * File <b>ServerReactor</b> located on fr.xharos.reactor
 * ServerReactor is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 04/07/2017 at 00:05
 * @since 0.0.2
 */
public class ServerReactor extends Reactor implements Runnable {

    private final List<ClientChannel>      channelsReadyToWrite;
    private final Map<Long, ClientChannel> channels;
    private final int                      byteBufferSize;
    private       ServerSocketChannel      channel;
    private       Selector                 demultiplexer;
    private       ReactorServerWriter      writer;
    private       long                     socketCounter;

    ServerReactor(InetSocketAddress adress, Side side) throws IOException {
        super(adress, side);
        this.channels = new HashMap<>();
        this.channelsReadyToWrite = new ArrayList<>();
        this.byteBufferSize = 1024;
        this.socketCounter = 0;
        init();
    }

    void init() throws IOException {
        this.channel = ServerSocketChannel.open();
        this.demultiplexer = Selector.open();
        this.writer = new ReactorServerWriter();
        channel.configureBlocking(false);
    }

    @Override
    void start(InetSocketAddress address) throws IOException {
        channel.bind(address);
        channel.register(demultiplexer, SelectionKey.OP_ACCEPT);
        Thread reactor = new Thread(this);
        reactor.setName("Main_Reactor_Server_Thread");
        reactor.start();

        Thread writerThread = new Thread(writer);
        writerThread.setName("Writer_Reactor_Server_Thread");
        writerThread.start();
    }

    @Override
    void close() throws Exception {
        //TODO maybe check if packets has been write
        run = false;
        channels.forEach((id, chan) -> {
            try {
                chan.getChannel().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        channels.clear();
        channelsReadyToWrite.clear();
        channel.close();
        this.socketCounter = 0;
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void subscribeBuffers(ClientChannel dest, ByteBuffer... buffers) {
        for (ByteBuffer buffer : buffers) {
            dest.subscribeBuffer(buffer);
        }
        try {
            dest.getChannel().register(demultiplexer, SelectionKey.OP_WRITE, dest.getId());
        } catch (ClosedChannelException e) {
            kick(dest.getId());
        }
        demultiplexer.wakeup();
    }

    @Override
    public void run() {
        while (run) {
            int selectednum = 0;
            try {
                selectednum = demultiplexer.select();
                if (selectednum > 0) {
                    Set<SelectionKey> readyKeys = demultiplexer.selectedKeys();
                    Iterator<SelectionKey> iterator = readyKeys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        iterator.remove();
                        try {
                            if (!key.isValid()) {
                                kick(key);
                                continue;
                            }
                            if (key.isAcceptable()) {
                                ClientChannel clientChannel = new ClientChannel(channel.accept(), socketCounter, byteBufferSize);
                                socketCounter++;
                                channels.put(clientChannel.getId(), clientChannel);
                                if (!getHandler(Type.ACCEPT).handle(clientChannel))
                                    kick(key);
                                clientChannel.getChannel().register(demultiplexer, SelectionKey.OP_WRITE, clientChannel.getId());
                            }
                            if (key.isReadable()) {
                                ClientChannel clientChannel = channels.get((long) key.attachment());
                                if (!getHandler(Type.READ).handle(clientChannel))
                                    kick(key);
                            }
                            if (key.isWritable()) {
                                ClientChannel clientChannel = channels.get((long) key.attachment());
                                writer.subscribeChannel(clientChannel);
                                clientChannel.getChannel().register(demultiplexer, SelectionKey.OP_READ, clientChannel.getId());
                            }
                        } catch (CancelledKeyException e) {
                            kick(key);
                        }
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                break;
            }
        }
    }

    void kick(SelectionKey key) {
        if (key.attachment() != null) {
            kick((long) key.attachment());
        }
        key.cancel();
    }

    void kick(long id) {
        ClientChannel chan = channels.remove(id);
        if (chan != null)
            getHandler(Type.DICSONNECT).handle(chan);
    }

    private class ReactorServerWriter implements Runnable {

        private Queue<ClientChannel> chansReadyToWrite = new ConcurrentLinkedQueue<>();

        void subscribeChannel(ClientChannel channel) {
            chansReadyToWrite.offer(channel);
        }

        @Override
        public void run() {
            while (run) {
                ClientChannel channel = chansReadyToWrite.poll();
                while (channel != null) {
                    if (!getHandler(Type.WRITE).handle(channel))
                        kick(channel.getId());
                    channel = chansReadyToWrite.poll();
                }
            }
        }
    }
}
