package fr.xharos.reactor;

import com.sun.xml.internal.ws.api.message.Packet;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * File <b>ClientChannel</b> located on fr.xharos.reactor
 * ClientChannel is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 23:40
 * @since 0.0.2
 */
public class ClientChannel {

    private final ByteBuffer                        readerBuffer;
    private final ConcurrentLinkedQueue<ByteBuffer> buffers;
    private final SocketChannel                     channel;
    private final long                              id;
    private final LinkedList<Packet>                packets;

    protected ClientChannel(SocketChannel channel, long id, int byteBufferSize) {
        this.channel = channel;
        this.packets = new LinkedList<>();
        this.id = id;
        this.buffers = new ConcurrentLinkedQueue<>();
        this.readerBuffer = ByteBuffer.allocate(byteBufferSize);
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public ByteBuffer getReaderBuffer() {
        return readerBuffer;
    }

    public long getId() {
        return id;
    }

    protected void subscribeBuffer(ByteBuffer data) {
        buffers.add(data);
    }

    protected boolean hasData() {
        return !buffers.isEmpty();
    }

    public ConcurrentLinkedQueue<ByteBuffer> getBuffers() {
        return buffers;
    }
}
