package fr.xharos.reactor;

import fr.xharos.functional.Disposable;
import fr.xharos.functional.Identifier;
import fr.xharos.reactor.ReactorFactory.Side;
import fr.xharos.reactor.handler.ReactorHandler;
import fr.xharos.reactor.handler.Type;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * File <b>Reactor</b> located on fr.xharos.reactor
 * Reactor is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 23:39
 * @since 0.0.2
 */
public abstract class Reactor implements Disposable, Identifier<ReactorFactory.Side> {

    private final      ConcurrentHashMap<Type, ReactorHandler> handlers;
    private final      Properties                              properties;
    private final      InetSocketAddress                       address;
    private final      ReactorFactory.Side                     side;
    protected volatile boolean                                 run;

    Reactor(InetSocketAddress adress, ReactorFactory.Side side) {
        this.address = adress;
        this.side = side;
        this.run = false;
        this.properties = new Properties();
        this.handlers = new ConcurrentHashMap<>();
    }

    protected static Reactor open(InetSocketAddress address, ReactorFactory.Side side) {
        try {
            switch (side) {
                case CLIENT:
                    return new ClientReactor(address, side);
                case SERVER:
                    return new ServerReactor(address, side);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setOptions(String key, String value) {
        if (!run)
            properties.setProperty(key, value);
    }

    public Properties getProperties() {
        return properties;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setHandler(Type type, ReactorHandler handler) {
        handlers.put(type, handler);
    }

    public void pauseReactor() {
        run = false;
    }

    public ReactorHandler getHandler(Type type) {
        return handlers.get(type);
    }

    public void launchReactor() throws Exception {
            try {
                launchReactor(address);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void launchReactor(InetSocketAddress newAddress) throws Exception {
        if (handlers.size() != 4)
            throw new UnsupportedOperationException("Handlers cannot be null, need at least 4 differents handlers!");
        else {
            try {
                start(newAddress);
                run = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    abstract void start(InetSocketAddress address) throws IOException;

    abstract void close() throws Exception;

    public abstract void subscribeBuffers(ClientChannel dest, ByteBuffer... buffers);

    @Override
    public void dispose() throws Exception {
        close();
    }

    @Override
    public Side getIdentifier() {
        return side;
    }
}
