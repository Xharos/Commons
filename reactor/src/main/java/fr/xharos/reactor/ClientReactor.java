package fr.xharos.reactor;

import fr.xharos.reactor.ReactorFactory.Side;
import fr.xharos.reactor.handler.Type;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * File <b>ClientReactor</b> located on fr.xharos.reactor
 * ClientReactor is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 23:53
 * @since 0.0.2
 */
public class ClientReactor extends Reactor implements Runnable {

    private final SocketChannel channel;
    private final Selector      demultiplexer;
    private       ClientChannel clientChannel;

    ClientReactor(InetSocketAddress adress, Side side) throws IOException {
        super(adress, side);
        this.channel = SocketChannel.open();
        this.demultiplexer = Selector.open();
        channel.configureBlocking(false);
    }

    @Override
    void start(InetSocketAddress address) throws IOException {
        channel.connect(address);
        channel.register(demultiplexer, SelectionKey.OP_CONNECT);
        Thread t = new Thread(this);
        t.setName("Main_Reactor_Client_Thread");
        t.start();
    }

    @Override
    void close() throws Exception {
        channel.close();
    }

    @Override
    public void subscribeBuffers(ClientChannel dest, ByteBuffer... buffers) {
        //Discard channel instance...
        for (ByteBuffer buffer : buffers) {
            dest.subscribeBuffer(buffer);
        }
        try {
            channel.register(demultiplexer, SelectionKey.OP_WRITE);
            demultiplexer.wakeup();
        } catch (IOException e) {
            disconnect();
        }
    }

    void disconnect() {
        try {
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (run) {
            int selectednum = 0;
            try {
                selectednum = demultiplexer.select();
                if (selectednum > 0) {
                    Set<SelectionKey> readyKeys = demultiplexer.selectedKeys();
                    Iterator<SelectionKey> iterator = readyKeys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        iterator.remove();
                        try {
                            if (!key.isValid()) {
                                close();
                                continue;
                            }

                            if (key.isConnectable()) {
                                this.clientChannel = new ClientChannel(channel, 0, 1024);
                                if (!getHandler(Type.ACCEPT).handle(clientChannel))
                                    disconnect();
                                channel.register(demultiplexer, SelectionKey.OP_WRITE);
                            }
                            if (key.isReadable()) {
                                if (!getHandler(Type.READ).handle(clientChannel))
                                    disconnect();
                            }
                            if (key.isWritable()) {
                                if (!getHandler(Type.WRITE).handle(clientChannel))
                                    disconnect();
                                clientChannel.getChannel().register(demultiplexer, SelectionKey.OP_READ);
                            }
                        } catch (CancelledKeyException e) {
                            disconnect();
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                break;
            }
        }
    }
}
