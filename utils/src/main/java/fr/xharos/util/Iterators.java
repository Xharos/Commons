package fr.xharos.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * File <b>Iterators</b> located on fr.xharos.util
 * Iterators is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 02/07/2017 at 00:07
 * @since 0.0.1
 */
public class Iterators {

    private static final Iterator<?> EMPTY_ITERATOR = new EmptyIterator();

    private Iterators() {
    }

    /**
     * Get an empty iterator , it contain nothing
     *
     * @param <T> the handler of the iterator
     * @return an empty iterator
     */
    @SuppressWarnings("unchecked")
    public static <T> Iterator<T> empty() {
        return (Iterator<T>) EMPTY_ITERATOR;
    }

    /**
     * Get an iterator of the provided values
     *
     * @param values values with the iterator , must contains
     * @param <T>    the handler of the iterator
     * @return an iterator of the provided values
     */
    @SafeVarargs
    public static <T> Iterator<T> of(T... values) {
        return new ArrayIterator<>(values);
    }

    /**
     * Add all iterator value in an array
     *
     * @param iterator the iterator to get the values
     * @param array    the array to add value
     * @param <T>      the handler of the iterator
     * @return an array with iterator value
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] toArray(Iterator<T> iterator, T[] array) {
        for (int i = 0; iterator.hasNext() && i < array.length; i++)
            array[i] = iterator.next();
        return array;
    }

    /**
     * Transform an iterator into a sorted iterator
     *
     * @param it         the iterator which must be sorted
     * @param comparator the comparator to sort the iterator
     * @param <T>        the handler of the iterator
     * @return a sorted iterator
     */
    public static <T> Iterator<T> sortedIterator(Iterator<T> it, Comparator<? super T> comparator) {
        List<T> list = new ArrayList<>();
        while (it.hasNext())
            list.add(it.next());

        Collections.sort(list, comparator);
        return list.iterator();
    }

    private static class EmptyIterator implements Iterator<Object> {

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Object next() {
            throw new NoSuchElementException();
        }
    }

    private static class ArrayIterator<T> implements Iterator<T> {

        private final    T[] array;
        private volatile int i;

        public ArrayIterator(T[] array) {
            this.array = array;
            this.i = 0;
        }

        @Override
        public boolean hasNext() {
            return i < array.length;
        }

        @Override
        public T next() {
            if (!hasNext())
                throw new NoSuchElementException();
            else
                return array[i++];
        }
    }
}
