package fr.xharos.test;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.junit.Before;
import org.junit.Test;

/**
 * File <b>Bench</b> located on fr.xharos.test
 * Bench is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 13/07/2017 at 00:06
 */
public class Bench {

    private static final Random                     random       = new Random();
    private              ConcurrentMap<Byte, Class> packetClass  = new ConcurrentHashMap<>();
    private              ConcurrentIndexMap<Class>  packetClass2 = new ConcurrentIndexMap<>();

    @Before
    public void addElements() {
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            packetClass.put(i, Bench.class);
            packetClass2.put(i+128, Bench.class);
        }
        init();
    }
    
    @Test
    public void testGet() {
        packetClass2.put(10, String.class);
        System.out.println(packetClass2.get(10));
    }

    @Test
    public void bench() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 3000000; i++) {
            packetClass.get(random.nextInt(256 + Byte.MIN_VALUE));
        }
        System.out.println("ConcurrentMap took " + (System.currentTimeMillis() - start) + "ms!");

        start = System.currentTimeMillis();
        for (int i = 0; i < 3000000; i++) {
            packetClass2.get(random.nextInt(256));
        }
        System.out.println("ConcurrentIndexMap took " + (System.currentTimeMillis() - start) + "ms!");
    }

    private void init() {
        for (int i = 0; i < 30000; i++) {
            packetClass.get(random.nextInt(256 + Byte.MIN_VALUE));
        }
        for (int i = 0; i < 30000; i++) {
            packetClass2.get(random.nextInt(256 + Byte.MIN_VALUE));
        }
    }
}
