package fr.xharos.test;

import fr.xharos.packet.Protocol;
import fr.xharos.packet.ProtocolDirection;
import fr.xharos.packet.ProtocolDirection.Side;
import fr.xharos.packet.event.PacketEvent;
import fr.xharos.packet.event.PacketHandler;
import fr.xharos.packet.nio.NetInput;
import fr.xharos.packet.nio.NetOutput;
import fr.xharos.packet.nio.buffer.ByteBufferNetInput;
import fr.xharos.packet.nio.buffer.ByteBufferNetOutput;
import fr.xharos.packet.packet.Packet;
import fr.xharos.packet.protocol.PacketType;
import fr.xharos.test.PacketTest.Packets.List;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * File <b>PacketTest</b> located on fr.xharos.test
 * PacketTest is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 12:05
 * @since 0.1.0
 */
public class PacketTest {

    private static final String message = "test";
    private static Protocol protocol;

    @BeforeClass
    public static void init() {
        protocol = new TestProtocol("TEST", ProtocolDirection.Side.SERVER, new InetSocketAddress("localhost", 12345));
    }

    @Test
    public void testRegisterPacket() {
        protocol.register(List.BASIC_PACKET);
        Assert.assertTrue(BasicPacket.class.equals(protocol.get(List.BASIC_PACKET)));
    }

    @Test
    public void testInvokePacket() {
        testRegisterPacket();
        try {
            Packet packet = protocol.invoke(List.BASIC_PACKET.getPacketId());
            Assert.assertTrue(packet.getType() == List.BASIC_PACKET);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testHandler() {
        testRegisterPacket();
        try {
            protocol.subscribeHandler(Packets.List.BASIC_PACKET, new PacketHandler<BasicPacket>() {
                @Override
                public void handle(PacketEvent<BasicPacket> event) {
                    event.getPacket().message.equalsIgnoreCase(message);
                }
            });

            BasicPacket packet = (BasicPacket) protocol.invoke(List.BASIC_PACKET.getPacketId());

            //simulate TCP call
            ByteBufferNetOutput output = new ByteBufferNetOutput(ByteBuffer.allocateDirect(1024));
            output.writeString(message);
            output.getByteBuffer().flip();

            packet.read(new ByteBufferNetInput(output.getByteBuffer()));
            packet.handle(protocol);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    public static class Packets<T extends Packet> implements PacketType<T> {

        private final byte                   id;
        private final Class<T>               packetClass;
        private final ProtocolDirection.Side side;

        Packets(byte id, Class<T> packetClass, ProtocolDirection.Side side) {
            this.id = id;
            this.packetClass = packetClass;
            this.side = side;
        }

        @Override
        public byte getPacketId() {
            return id;
        }

        @Override
        public ProtocolDirection.Side getSide() {
            return side;
        }

        @Override
        public Class<T> getPacketClass() {
            return packetClass;
        }

        public static class List {

            public static Packets<BasicPacket> BASIC_PACKET = new Packets((byte) 0, BasicPacket.class, Side.BOTH);
        }
    }

    public static class BasicPacket implements Packet {

        private String message;

        @Override
        public void read(NetInput input) throws IOException {
            this.message = input.readString();
        }

        @Override
        public NetOutput write(NetOutput output) throws IOException {
            output.writeString(message);
            return output;
        }

        @Override
        public boolean validatePacketSize(int size) {
            return true;//because String
        }

        @Override
        public PacketType getType() {
            return List.BASIC_PACKET;
        }

        @Override
        public void handle(Protocol prot) {
            prot.handle(new PacketEvent<>(this, System.currentTimeMillis()));
        }

    }

    public static class TestProtocol extends Protocol<String> {

        public TestProtocol(String protocolName, ProtocolDirection.Side currentSide, InetSocketAddress localAddress) {
            super(protocolName, currentSide, localAddress);
        }

        @Override
        public void connect() throws Exception {
            System.out.println("TestProtocol.connect");
        }

        @Override
        public void close() throws Exception {
            System.out.println("TestProtocol.close");
        }
    }
}
