package fr.xharos.packet.nio.buffer;

import fr.xharos.packet.nio.NetOutput;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

/**
 * File <b>ByteBufferNetOutput</b> located on fr.xharos.packet.nio.buffer
 * ByteBufferNetOutput is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 11:47
 * @since 0.1.0
 */
public class ByteBufferNetOutput implements NetOutput {

    private final ByteBuffer buffer;

    public ByteBufferNetOutput(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public ByteBuffer getByteBuffer() {
        return buffer;
    }

    @Override
    public void writeBoolean(boolean b) throws IOException {
        buffer.put(b ? (byte) 1 : 0);
    }

    @Override
    public void writeByte(int b) throws IOException {
        buffer.put((byte) b);
    }

    @Override
    public void writeShort(int s) throws IOException {
        buffer.putShort((short) s);
    }

    @Override
    public void writeChar(int c) throws IOException {
        buffer.putChar((char) c);
    }

    @Override
    public void writeInt(int i) throws IOException {
        buffer.putInt(i);
    }

    @Override
    public void writeVarInt(int i) throws IOException {
        while ((i & ~0x7F) != 0) {
            writeByte((i & 0x7F) | 0x80);
            i >>>= 7;
        }
        writeByte(i);
    }

    @Override
    public void writeLong(long l) throws IOException {
        buffer.putLong(l);
    }

    @Override
    public void writeBigInteger(BigInteger bInt) throws IOException {
        if (bInt == null)
            throw new IllegalArgumentException("BigInteger cannot be null!");
        byte[] b = bInt.toByteArray();
        if (b[0] == 0)
            Arrays.copyOfRange(b, 1, b.length);
        writeVarInt(b.length);
        writeBytes(b);
    }

    @Override
    public void writeVarLong(long l) throws IOException {
        while ((l & ~0x7F) != 0) {
            writeByte((int) (l & 0x7F) | 0x80);
            l >>>= 7;
        }
        writeByte((int) l);
    }

    @Override
    public void writeFloat(float f) throws IOException {
        buffer.putFloat(f);
    }

    @Override
    public void writeDouble(double d) throws IOException {
        buffer.putDouble(d);
    }

    @Override
    public void writeBytes(byte b[]) throws IOException {
        buffer.put(b);
    }

    @Override
    public void writeBytes(byte b[], int length) throws IOException {
        buffer.put(b, 0, length);
    }

    @Override
    public void writeShorts(short[] s) throws IOException {
        writeShorts(s, s.length);
    }

    @Override
    public void writeShorts(short[] s, int length) throws IOException {
        for (int index = 0; index < length; index++)
            writeShort(s[index]);
    }

    @Override
    public void writeInts(int[] i) throws IOException {
        writeInts(i, i.length);
    }

    @Override
    public void writeInts(int[] i, int length) throws IOException {
        for (int index = 0; index < length; index++)
            writeInt(i[index]);
    }

    @Override
    public void writeLongs(long[] l) throws IOException {
        writeLongs(l, l.length);
    }

    @Override
    public void writeLongs(long[] l, int length) throws IOException {
        for (int index = 0; index < length; index++)
            writeLong(l[index]);
    }

    @Override
    public void writeString(String s) throws IOException {
        if (s == null)
            throw new IllegalArgumentException("String cannot be null!");
        byte[] bytes = s.getBytes("UTF-8");
        if (bytes.length > 32767)
            throw new IOException("String too big (was " + s.length() + " bytes encoded, max " + 32767 + ")");
        else {
            writeVarInt(bytes.length);
            writeBytes(bytes);
        }
    }

    @Override
    public void writeUUID(UUID uuid) throws IOException {
        writeLong(uuid.getMostSignificantBits());
        writeLong(uuid.getLeastSignificantBits());
    }
}
