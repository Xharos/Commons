package fr.xharos.packet;

import fr.xharos.functional.Disposable;
import fr.xharos.functional.Invoke;
import fr.xharos.manager.AbstractManager;
import fr.xharos.packet.packet.Packet;
import fr.xharos.packet.protocol.PacketType;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

/**
 * File <b>ProtocolDirection</b> located on fr.xharos.packet
 * ProtocolDirection is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 12:03
 * @since 0.1.0
 */
public class ProtocolDirection implements AbstractManager<PacketType, Class<? extends Packet>>, Invoke<Byte, Packet>, Disposable {

    private final ConcurrentMap<Byte, Class<? extends Packet>> packets;
    private final Side                                         localSide;

    ProtocolDirection(Side currentSide) {
        this.localSide = currentSide;
        this.packets = new ConcurrentHashMap();
    }

    @Override
    public void register(PacketType key, Class<? extends Packet> value) {
        if (key.getSide() == Side.BOTH || key.getSide() != localSide)
            packets.putIfAbsent(key.getPacketId(), value);
    }

    @Override
    public Class<? extends Packet> get(PacketType key) {
        return packets.get(key.getPacketId());
    }

    @Override
    public Stream<Class<? extends Packet>> getAssociatedResource() {
        return packets.values().stream();
    }

    @Override
    public Packet invoke(Byte header) throws IOException {
        Class<? extends Packet> packetClass = packets.get(header);
        try {
            Packet packet = packetClass.getConstructor().newInstance();
            if (packet.getType().getSide() == Side.BOTH || packet.getType().getSide() != localSide)
                return packet;
            else
                throw new IllegalArgumentException("Bad protocol side, expected " + (localSide == Side.SERVER ? "CLIENT" : "SERVER"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean headerExist(byte b) {
        return packets.containsKey(b);
    }

    public Side getLocalSide() {
        return localSide;
    }

    @Override
    public String getIdentifier() {
        return getClass().getSimpleName();
    }

    @Override
    public void dispose(PacketType resource) throws Exception {
        packets.remove(resource.getPacketId());
    }

    @Override
    public void dispose() throws Exception {
        packets.clear();
    }

    public static enum Side {
        CLIENT,
        SERVER,
        BOTH;
    }
}
