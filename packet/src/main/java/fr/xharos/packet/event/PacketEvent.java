package fr.xharos.packet.event;

import fr.xharos.packet.packet.Packet;

/**
 * File <b>PacketEvent</b> located on fr.xharos.packet.event
 * PacketEvent is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 12/07/2017 at 21:23
 * @since 0.2.5
 */
public class PacketEvent<T extends Packet> {

    private final T    packet;
    private final long handleTime;

    public PacketEvent(T packet, long handleTime) {
        this.packet = packet;
        this.handleTime = handleTime;
    }

    public T getPacket() {
        return packet;
    }

    public long getHandleTime() {
        return handleTime;
    }
}
