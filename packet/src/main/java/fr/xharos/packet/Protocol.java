package fr.xharos.packet;

import fr.xharos.functional.Disposable;
import fr.xharos.functional.Invoke;
import fr.xharos.manager.AbstractMultipleManager;
import fr.xharos.packet.event.PacketEvent;
import fr.xharos.packet.event.PacketHandler;
import fr.xharos.packet.nio.Iprotocol;
import fr.xharos.packet.packet.Packet;
import fr.xharos.packet.protocol.PacketType;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;

/**
 * File <b>Protocol</b> located on fr.xharos.packet
 * Protocol is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 12:31
 * @since 0.1.0
 */
public abstract class Protocol implements AbstractMultipleManager<PacketType, Class<? extends Packet>>, Iprotocol, Invoke<Byte, Packet>, Disposable {

    private final   String            protocolName;
    private final   ProtocolDirection direction;
    public ConcurrentMap<Byte, PacketHandler> packetConsumer = new ConcurrentHashMap<>();

    public Protocol(String protocolName, ProtocolDirection.Side currentSide) {
        this.protocolName = protocolName;
        this.direction = new ProtocolDirection(currentSide);
    }

    @Override
    public synchronized Packet invoke(Byte key) throws IOException {
        if (direction.headerExist(key))
            return direction.invoke(key);
        else
            throw new IOException("Empty or non registered header");
    }

    @Override
    public String getIdentifier() {
        return protocolName;
    }

    public ProtocolDirection.Side getLocalSide() {
        return direction.getLocalSide();
    }

    @Override
    public void dispose(PacketType resource) throws Exception {
        direction.dispose(resource);
    }

    public <T extends Packet> void subscribeHandler(PacketType<T> type, PacketHandler<T> handler) {
        packetConsumer.putIfAbsent(type.getPacketId(), handler);
    }

    public <T extends Packet> void handle(PacketEvent<T> event) {
        byte id = event.getPacket().getType().getPacketId();
        PacketHandler<T> handler = packetConsumer.get(id);
        if (handler != null)
            handler.handle(event);
    }

    public void register(PacketType key) {
        direction.register(key, key.getPacketClass());
    }

    /**
     * register this packet
     *
     * @param key   a PacketType
     * @param value an implementation of Packet interface
     */
    @Override
    public void register(PacketType key, Class<? extends Packet> value) {
        if (key.getPacketClass().equals(value))
            direction.register(key, value);
    }

    @Override
    public Class<? extends Packet> get(PacketType key) {
        return direction.get(key);
    }

    @Override
    public void registerAll(PacketType[] values) {
        for (PacketType value : values)
            direction.register(value, value.getPacketClass());
    }

    @Override
    public Stream<Class<? extends Packet>> getAssociatedResource() {
        return direction.getAssociatedResource();
    }

    @Override
    public void dispose() throws Exception {
        close();
        direction.dispose();
    }

}
