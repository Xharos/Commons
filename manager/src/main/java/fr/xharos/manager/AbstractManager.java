package fr.xharos.manager;

import fr.xharos.functional.Identifier;
import fr.xharos.functional.ResourceDisposable;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * File <b>AbstractManager</b> located on fr.xharos.manager
 * AbstractManager is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 14:13
 * @since 0.1.0
 */
public interface AbstractManager<K, V> extends ResourceDisposable<K>, Identifier<String> {

    /**
     * Register the given generic parameter
     *
     * @param key a generic var
     * @param value a generic var
     */
    void register(K key, V value);

    /**
     * Get the resource associated to this key, or else null
     *
     * @param key a generic key
     * @return a generic var if exist
     */
    V get(K key);

    /**
     * Get the resource associated to this key, or else null
     *
     * @param key          a generic key
     * @param defaultValue a generic key if the first one doesn't match anything
     * @return a generic var if exist
     * @see java.util.HashMap#getOrDefault(Object, Object)
     */
    default V getOrElse(K key, V defaultValue) {
        V value = get(key);
        return value == null ? defaultValue : value;
    }

    /**
     * Get the resource associated to this key, or else null
     *
     * @param key               a generic key
     * @param exceptionSupplier an exception to throw if the key doesn't match anything
     * @return a generic var if exist
     * @see java.util.Optional#orElseThrow(Supplier)
     */
    default <X extends Throwable> V getOrElseThrow(K key, Supplier<? extends X> exceptionSupplier) throws X {
        V value = get(key);
        if (value == null)
            throw exceptionSupplier.get();
        else
            return value;
    }

    /**
     * Get all registered resource {@link AbstractManager#register(Object, Object)}
     *
     * @return a {@link Stream} of registered resources
     */
    Stream<V> getAssociatedResource();

}