package fr.xharos.test;

import fr.xharos.logger.Logger;
import fr.xharos.logger.handler.DefaultConsoleHandler;

import java.util.logging.Level;

/**
 * File <b>TestLogger</b> located on fr.xharos.test
 * TestLogger is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 29/08/2017 at 17:47
 * @since 0.0.1
 */
public class TestLogger {

    public static void main(String[] args) {
        Logger                logger         = new Logger("TEST");
        DefaultConsoleHandler consoleHandler = new DefaultConsoleHandler(System.out);

        consoleHandler.setFormatter(logger.getDefaultFormatter());
        logger.registerLevel(new CustomLevel("CUSTOM-LEVEL", 1200));
        logger.addHandler(consoleHandler);
        logger.redirectSystemStream();

        logger.log("test");
        logger.log("test2");
        logger.debug("Debug");
        logger.error("Some error content");
        logger.log("custom-level", "Custom msg ;)");
        System.err.println("Error from System.err");
        System.out.println("Msg from System.out");
        logger.log(Level.FINE, "fine");
        logger.log(Level.FINER, "finer");
        logger.log(Level.FINEST, "finest");

        try {
            logger.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class CustomLevel extends Level {
        protected CustomLevel(String name, int value) {
            super(name, value);
        }
    }
}
