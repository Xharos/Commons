package fr.xharos.logger;

import fr.xharos.logger.level.DebugLevel;
import fr.xharos.logger.stream.LoggingOutputStream;

import java.io.PrintStream;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 * File <b>Logger</b> located on fr.xharos.logger
 * Logger is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 29/08/2017 at 17:32
 * @since 0.0.1
 */
public class Logger extends java.util.logging.Logger implements Log {

    private final DefaultFormatter             defaultFormatter;
    private final LogDispatcher                dispatcher;
    private final ConcurrentLinkedQueue<Level> customLevel;

    public Logger(String name) {
        super(name, null);
        setLevel(Level.ALL);
        this.dispatcher = new LogDispatcher(this);
        this.customLevel = new ConcurrentLinkedQueue<>();
        this.defaultFormatter = new DefaultFormatter();

        LogManager.getLogManager().addLogger(this);
        registerLevel(new DebugLevel("DEBUG"));
        dispatcher.start();
    }

    @Override
    public void log(String level, String msg) {
        Level logLevel = customLevel.stream().filter(lvl -> lvl.getName().equalsIgnoreCase(level)).findFirst().orElse(Level.INFO);
        log(logLevel, msg);
    }

    @Override
    public void log(String msg) {
        log(Level.INFO, msg);
    }

    @Override
    public void error(String msg) {
        log(Level.SEVERE, msg);
    }

    @Override
    public void debug(String msg) {
        log("DEBUG", msg);
    }

    @Override
    public void registerLevel(Level level) {
        if (!customLevel.stream().anyMatch(lvl -> lvl.equals(level)))
            customLevel.add(level);
    }

    @Override
    public void log(LogRecord record) {
        dispatcher.queue(record);
    }

    @Override
    public void dispose() throws Exception {
        dispatcher.dispose();
    }

    public void redirectSystemStream() {
        System.setOut(new PrintStream(new LoggingOutputStream(this, Level.INFO), true));
        System.setErr(new PrintStream(new LoggingOutputStream(this, Level.SEVERE), true));
    }

    public DefaultFormatter getDefaultFormatter() {
        return defaultFormatter;
    }

    void doLog(LogRecord record) {
        super.log(record);
    }

}
