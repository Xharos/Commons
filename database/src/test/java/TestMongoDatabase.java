import fr.xharos.database.collection.Collection;
import fr.xharos.database.collection.Filter;
import fr.xharos.database.mongodb.MongoDBDatabase;
import fr.xharos.serialization.annotations.SerializedName;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.UUID;

/**
 * File <b>TestMongoDatabase</b> located on PACKAGE_NAME
 * TestMongoDatabase is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 02/07/2017 at 00:26
 */
public class TestMongoDatabase {

    private MongoDBDatabase database;

    public void connect() {
        this.database = new MongoDBDatabase(Arrays.asList(InetSocketAddress.createUnresolved("localhost", 12345)), "user", "pass", "defaultDatabase");
        try {
            database.connect();
            Collection<Player> playersDocument = database.get("uhc_player", Player.class); //Get collection
            Player             p               = new Player("Gogume1er");

            playersDocument.putOrUpdate(Filter.eq("name", "gogume1er"), p);

            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class Player {

        @SerializedName("uhc_player_id")
        private final UUID   id;
        @SerializedName("uhc_player_name")
        private       String name;

        public Player(String name) {
            this.name = name;
            this.id = UUID.randomUUID();
        }
    }
}
