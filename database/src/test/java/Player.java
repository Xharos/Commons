import fr.xharos.database.redis.DatabaseFormatable;
import fr.xharos.serialization.annotations.SerializedName;
import fr.xharos.serialization.annotations.SerializedName.Identifier;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * File <b>Player</b> located on PACKAGE_NAME
 * Player is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 14:27
 * @since 0.0.2
 */
public class Player implements DatabaseFormatable<Map<String, String>, Player> {

    @SerializedName(value = "id", identifier = Identifier.PRIMARY_KEY)
    private final UUID   uuid;
    public        int    money;
    public        String name;

    public Player(String uuid) {
        this.uuid = UUID.fromString(uuid);
    }

    @Override
    public Player adaptTo(Map<String, String> in) {
        this.money = Integer.valueOf(in.get("money"));
        this.name = in.get("name");
        return this;
    }

    @Override
    public Map<String, String> adaptFrom() {
        Map<String, String> datas = new HashMap<>();
        datas.put("money", String.valueOf(money));
        datas.put("name", name);
        return datas;
    }
}
