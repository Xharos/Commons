import fr.xharos.database.redis.RedisDatabase;
import org.junit.Assert;
import org.junit.Test;

/**
 * File <b>TestRedisDatabase</b> located on PACKAGE_NAME
 * TestRedisDatabase is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 12:38
 * @since 0.0.2
 */
public class TestRedisDatabase {

    private RedisDatabase database;

    @Test
    public void connect() {
        this.database = new RedisDatabase(6379, "localhost", "", "password");
        try {
            database.connect();

            Player p = new Player("172bed8d-6757-41a9-9ec6-3e0a771fe974");
            p.name = "Xharos";
            p.money = 56;

            database.save(p);

            Player p2 = new Player("172bed8d-6757-41a9-9ec6-3e0a771fe974");

            database.load(p2);
            database.close();

            Assert.assertTrue(p.money == p2.money);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
