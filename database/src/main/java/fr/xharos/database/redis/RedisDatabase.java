package fr.xharos.database.redis;

import fr.xharos.database.AbstractDatabase;
import fr.xharos.database.DaoQueries;
import fr.xharos.database.DatabaseProperties;
import fr.xharos.serialization.annotations.SerializedName;
import fr.xharos.serialization.annotations.SerializedName.Identifier;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.util.Map;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * File <b>RedisDatabase</b> located on fr.xharos.database.redis
 * RedisDatabase is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 02/07/2017 at 22:13
 * @since 0.0.2
 */
public class RedisDatabase extends AbstractDatabase<Jedis, JedisPoolConfig> implements DaoQueries {

    private JedisPool pool;

    public RedisDatabase(int port, String host, String userName, String password) {
        super(port, host, userName, password);
    }

    @Override
    public void connect() throws Exception {
        InetSocketAddress serverSocketAddress = serverAddress.get(0);
        if (propertiesExist()) {
            JedisPoolConfig config = buildOptions(properties.get());
            this.pool = new JedisPool(config, serverSocketAddress.getHostName(), serverSocketAddress.getPort(), 2000, password);
        }
        /**2000 is default jedis timeout
         * @see  JedisPool*/
        this.pool = new JedisPool(new JedisPoolConfig(), serverSocketAddress.getHostName(), serverSocketAddress.getPort(), 2000, password);
    }

    @Override
    public void close() throws Exception {
        pool.close();
    }

    @Override
    public Jedis getConnection() {
        if (pool == null)
            try {
                connect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        return pool.getResource();
    }

    @Override
    protected JedisPoolConfig buildOptions(DatabaseProperties properties) {
        return null;
    }

    @Override
    public <O> void save(DatabaseFormatable<Map<String, String>, O> instance) {
        Field primaryField = getPrimarySerializerField(instance);
        try {
            primaryField.setAccessible(true);
            String uniqueKey = primaryField.get(instance).toString();
            primaryField.setAccessible(false);
            save(instance, uniqueKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public <O> void save(DatabaseFormatable<Map<String, String>, O> instance, String uniqueKey) {
        if (uniqueKey != null && !uniqueKey.isEmpty()) {
            Map<String, String> datas = instance.adaptFrom();
            if (!datas.isEmpty()) {
                System.out.println("Save " + datas + " with uniqueKey = " + uniqueKey);
                Jedis jedis = getConnection();
                jedis.hmset(uniqueKey, datas);
                jedis.close();
            }
        }
    }

    @Override
    public <O> void load(DatabaseFormatable<Map<String, String>, O> instance) {
        Field primaryField = getPrimarySerializerField(instance);
        try {
            primaryField.setAccessible(true);
            String uniqueKey = primaryField.get(instance).toString();
            primaryField.setAccessible(false);
            load(instance, uniqueKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public <O> void load(DatabaseFormatable<Map<String, String>, O> instance, String uniqueKey) {
        if (uniqueKey != null) {
            Jedis jedis = getConnection();
            Map<String,String> datas = jedis.hgetAll(uniqueKey);
            System.out.println("Load " + datas + " with uniqueKey = " + uniqueKey);
            if(!datas.isEmpty())
            instance.adaptTo(datas);
            jedis.close();
        }
    }

    private <T> Field getPrimarySerializerField(T instance) {
        for (Field field : instance.getClass().getDeclaredFields()) {
            SerializedName serialized = field.getAnnotation(SerializedName.class);
            if (serialized != null)
                if (serialized.identifier() == Identifier.PRIMARY_KEY)
                    return field;
        }
        throw new UnsupportedOperationException("Class does not contains SerializedName field with primary key tag!");
    }
}
