package fr.xharos.database;

import fr.xharos.database.collection.Collection;
import fr.xharos.serialization.serializer.Serializer;

/**
 * File <b>DatabaseCollections</b> located on fr.xharos.database
 * DatabaseCollections is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 01/07/2017 at 22:27
 * @since 0.0.1
 */
public interface DatabaseCollections {

    /**
     * Delete the provided collection
     *
     * @param collection the collection to delete
     */
    void drop(Collection<?> collection);

    /**
     * Get the collections in the database
     *
     * @return the collections in the database
     */
    Collection<?>[] getCollections();

    /**
     * <p>
     * Get the a collection using the provided id and create it if there is no collection for the id
     * </p>
     *
     * @param id
     *            the name of the collection
     * @param clazz
     *            the handler of the documents in collection
     * @param <T>
     *            the handler of the collection
     * @return the collection
     */
    <T> Collection<T> get(String id, Class<T> clazz) throws IllegalStateException;

    /**
     * Get or create a collection using the provided id
     *
     * @param id
     *            the id
     * @param serializer
     *            the serializer for the collection handler
     * @param <T>
     *            the handler of collection
     * @return the collection
     */
    <T> Collection<T> get(String id, Class<T> clazz, Serializer<T> serializer) throws IllegalStateException;

}
