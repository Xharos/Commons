package fr.xharos.database.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.xharos.database.AbstractDatabase;
import fr.xharos.database.DatabaseCollections;
import fr.xharos.database.DatabaseProperties;
import fr.xharos.database.collection.Collection;
import fr.xharos.serialization.factory.SerializationFactory;
import fr.xharos.serialization.impl.unsafe.UnsafeSerializationFactory;
import fr.xharos.serialization.serializer.Serializer;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * File <b>MongoDBDatabase</b> located on fr.xharos.database.mongodb
 * MongoDBDatabase is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 * @author Sceat {@literal <sceat@aresrpg.fr>}
 *         Created the 01/07/2017 at 22:03
 * @since 0.0.1
 */
public class MongoDBDatabase extends AbstractDatabase<MongoDatabase, MongoClientOptions> implements DatabaseCollections {

    private final Map<String, MongoDBCollection> collections;
    private final SerializationFactory           serializerFactory;
    private       MongoClient                    client;
    private       MongoDatabase                  database;

    public MongoDBDatabase(List<InetSocketAddress> serverAddress, String userName, String password, String databaseName) {
        super(serverAddress, userName, password, databaseName);
        this.collections = new HashMap<>();
        this.serializerFactory = new UnsafeSerializationFactory();
    }

    @Override
    public void connect() throws Exception {
        List<ServerAddress> mongoServerAddress = new ArrayList<>();
        serverAddress.forEach(address -> {
            mongoServerAddress.add(new ServerAddress(address.getHostName(), address.getPort()));
        });
        if (propertiesExist()) {
            MongoClientOptions connectionOptions = buildOptions(properties.get());
            this.client = new MongoClient(mongoServerAddress, Collections.singletonList(MongoCredential.createCredential(userName, databaseName, password.toCharArray())), connectionOptions);
            this.database = client.getDatabase(databaseName);
        } else {
            this.client = new MongoClient(mongoServerAddress, Collections.singletonList(MongoCredential.createCredential(userName, databaseName, password.toCharArray())));
            this.database = client.getDatabase(databaseName);
        }
    }

    @Override
    public void close() throws Exception {
        client.close();
    }

    @Override
    public MongoDatabase getConnection() {
        if (database == null)
            try {
                connect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        return database;
    }

    @Override
    public MongoClientOptions buildOptions(DatabaseProperties properties) {
        //TODO code this bitch :/
        return null;
    }

    @Override
    public void drop(Collection<?> collection) {
        ((MongoCollection) collection).drop();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<?>[] getCollections() {
        return collections.values().toArray((Collection<?>[]) new Object[collections.size()]);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> Collection<T> get(String id, Class<T> type) throws IllegalStateException {
        return get(id, type, serializerFactory.createSerializer(type));
    }

    @Override
    public <T> Collection<T> get(String id, Class<T> type, Serializer<T> serializer) throws IllegalStateException {
        if (database == null)
            throw new IllegalStateException("Unable to get the collection ! The database is not connected.");
        MongoDBCollection<T> collection = collections.get(id);
        if (collection != null)
            return collection;
        collection = new MongoDBCollection<>(database.getCollection(id), serializer, type);
        collections.put(id, collection);
        return collection;
    }
}
