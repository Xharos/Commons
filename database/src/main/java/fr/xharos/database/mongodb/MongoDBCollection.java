package fr.xharos.database.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import fr.xharos.database.collection.Collection;
import fr.xharos.database.collection.Filter;
import fr.xharos.database.mongodb.serialization.DocumentFormat;
import fr.xharos.serialization.serializer.Serializer;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;

/**
 * File <b>MongoDBCollection</b> located on fr.xharos.database.mongodb
 * MongoDBCollection is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 * @author Sceat {@literal <sceat@aresrpg.fr>}
 *         Created the 01/07/2017 at 23:14
 * @since 0.0.1
 */
public class MongoDBCollection<T> implements Collection<T> {

    public static final UpdateOptions UPSERT      = new UpdateOptions().upsert(true);
    public static final String        FIELD_MONGO = "MongoDBCollection";
    private final MongoCollection<Document> collection;
    private       Serializer<T>             serializer;
    private       Class<T>                  clazz;

    MongoDBCollection(MongoCollection<Document> collection, Serializer<T> serializer, Class<T> clazz) {
        this.collection = collection;
        this.serializer = serializer;
        this.clazz = clazz;
    }

    private static Bson toMongoDBFilter(Filter filter) {
        if (filter == null)
            return null;
        Bson mfilter = null;
        switch (filter.getType()) {
            case AND:
                mfilter = Filters.and(toMongoDBFilters((Filter[]) filter.getValue()));
                break;
            case OR:
                mfilter = Filters.or(toMongoDBFilters((Filter[]) filter.getValue()));
                break;
            case NOR:
                mfilter = Filters.nor(toMongoDBFilters((Filter[]) filter.getValue()));
                break;
            case IN:
                mfilter = Filters.in(filter.getName(), (Object[]) filter.getValue());
                break;
            case NOT_IN:
                mfilter = Filters.nin(filter.getName(), (Object[]) filter.getValue());
                break;
            case EQUALS:
                mfilter = Filters.eq(filter.getName(), filter.getValue());
                break;
            case GREATER:
                mfilter = Filters.gt(filter.getName(), filter.getValue());
                break;
            case GREATER_OR_EQUALS:
                mfilter = Filters.gte(filter.getName(), filter.getValue());
                break;
            case LESS:
                mfilter = Filters.lt(filter.getName(), filter.getValue());
                break;
            case LESS_OR_EQUALS:
                mfilter = Filters.lte(filter.getName(), filter.getValue());
                break;
            case EXISTS:
                mfilter = Filters.exists(filter.getName(), (Boolean) filter.getValue());
                break;
            case ARRAY_SIZE:
                mfilter = Filters.size(filter.getName(), (Integer) filter.getValue());
                break;
            case TEXT:
                mfilter = Filters.text((String) filter.getValue());
                break;
            case REGEX:
                mfilter = Filters.regex(filter.getName(), (String) filter.getValue());
                break;
            default:
                break;
        }
        return mfilter;
    }

    private static Bson[] toMongoDBFilters(Filter... filters) {
        Bson[] bfilters = new Bson[filters.length];
        for (int i = 0; i < bfilters.length; i++) {
            bfilters[i] = toMongoDBFilter(filters[i]);
        }
        return bfilters;
    }

    /**
     * @return the clazz
     */
    public Class<T> getClazz() {
        return clazz;
    }

    @Override
    public void put(T t) {
        try {
            Document document = new Document();
            serializer.serialize(document, t, DocumentFormat.INSTANCE);
            collection.insertOne(document);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Filter filter, T t) {
        try {
            Document document = new Document();
            serializer.serialize(document, t, DocumentFormat.INSTANCE);
            collection.updateOne(toMongoDBFilter(filter), new Document("$set", document));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putOrUpdate(Filter filter, T t) {
        try {
            Document document = new Document();
            serializer.serialize(document, t, DocumentFormat.INSTANCE);
            collection.updateOne(toMongoDBFilter(filter), new Document("$set", document), UPSERT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAll(Filter filter, T t) {
        try {
            Document document = new Document();
            serializer.serialize(document, t, DocumentFormat.INSTANCE);
            collection.updateMany(toMongoDBFilter(filter), new Document("$set", document));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putOrUpdateAll(Filter filter, T t) {
        try {
            Document document = new Document();
            serializer.serialize(document, t, DocumentFormat.INSTANCE);
            collection.updateMany(toMongoDBFilter(filter), new Document("$set", document), UPSERT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T[] find(Filter filter, int limit) {
        if (limit == 0)
            return (T[]) Array.newInstance(getClazz(), 0);
        try {
            T[]                   found = (T[]) Array.newInstance(getClazz(), limit);
            MongoCursor<Document> cursor;
            if (filter == null)
                cursor = collection.find().iterator();
            else
                cursor = collection.find(toMongoDBFilter(filter)).iterator();
            int i;
            for (i = 0; cursor.hasNext() && i < limit; i++)
                found[i] = serializer.deserialize(cursor.next(), DocumentFormat.INSTANCE);
            return Arrays.copyOf(found, i);
        } catch (IOException e) {
            e.printStackTrace();
            return (T[]) Array.newInstance(getClazz(), 0);
        }
    }

    @Override
    public T[] sorted(String fieldname, int limit) {
        if (limit == 0)
            return (T[]) Array.newInstance(getClazz(), 0);
        try {
            T[]                   found = (T[]) Array.newInstance(getClazz(), limit);
            MongoCursor<Document> cursor;
            cursor = collection.find().sort(Filters.exists(fieldname)).iterator();
            int i;
            for (i = 0; cursor.hasNext() && i < limit; i++)
                found[i] = serializer.deserialize(cursor.next(), DocumentFormat.INSTANCE);
            return Arrays.copyOf(found, i);
        } catch (IOException e) {
            e.printStackTrace();
            return (T[]) Array.newInstance(getClazz(), 0);
        }
    }

    @Override
    public int remove(Filter filter, int limit) {
        MongoCursor<Document> cursor = collection.find(toMongoDBFilter(filter)).iterator();
        int                   i;
        for (i = 0; cursor.hasNext() && i < limit; i++)
            cursor.remove();
        return i;
    }

    @Override
    public long count() {
        return collection.count();
    }

    @Override
    public String getId() {
        return collection.getNamespace().getCollectionName();
    }

    public MongoCollection<Document> getCollection() {
        return collection;
    }

    @Override
    public boolean isEmpty() {
        return collection.count() == 0;
    }

    @Override
    public Iterator<T> iterator() {
        collection.find().map(v -> {
            try {
                return serializer.deserialize(v, DocumentFormat.INSTANCE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).iterator();
        return null;
    }
}
