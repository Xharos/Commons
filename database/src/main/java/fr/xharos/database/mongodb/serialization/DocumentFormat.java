package fr.xharos.database.mongodb.serialization;

import fr.xharos.serialization.Format;
import fr.xharos.serialization.SerializationContext;
import fr.xharos.serialization.TypeEnum;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.bson.Document;

/**
 * File <b>DocumentFormat</b> located on fr.xharos.database.mongodb.serialization
 * DocumentFormat is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 * @author Sceat {@literal <sceat@aresrpg.fr>}
 *         Created the 01/07/2017 at 23:23
 * @since 0.0.1
 */
public class DocumentFormat implements Format<Document, Document> {

    public static final DocumentFormat INSTANCE = new DocumentFormat();

    private DocumentFormat() {
    }

    @Override
    public void writeBegin(Document o) throws IOException {
        // Ignored
    }

    @Override
    public void writeValue(Document doc, String name, TypeEnum type, Object value, SerializationContext context) throws IOException {
        if (type == TypeEnum.OBJECT) {
            Document d = new Document();
            context.serialize(d, value, this);
            doc.put(name, d);
        } else if (type == TypeEnum.OBJECT_ARRAY) {
            int length = Array.getLength(value);
            Document[] d = new Document[length];
            for (int i = 0; i < length; i++)
                context.serialize(d[i] = new Document(), Array.get(value, i), this);
            doc.put(name, Arrays.asList(d));
        } else if (type.isArray()) {
            int length = Array.getLength(value);
            Object[] d = new Object[length];
            for (int i = 0; i < length; i++)
                d[i] = Array.get(value, i);
            doc.put(name, Arrays.asList(d));
        } else
            doc.put(name, value);
    }

    @Override
    public void writeBeginObject(Document o) throws IOException {
        // Ignored
    }

    @Override
    public void writeFieldSeparator(Document o, boolean b, boolean b1) throws IOException {
        // Ignored
    }

    @Override
    public void writeEndObject(Document o) throws IOException {
        // Ignored
    }

    @Override
    public void writeEnd(Document o) throws IOException {
        // Ignored
    }

    @Override
    public Object read(Document doc) throws IOException {
        Map<String, Object> map = new LinkedHashMap<>();
        for (Map.Entry<String, Object> e : doc.entrySet()) {
            if (e.getValue() instanceof Document) {
                map.put(e.getKey(), read((Document) e.getValue()));
            } else if (e.getValue() instanceof List) {
                List<Object> dd = (List<Object>) e.getValue();
                Object[] mmap = new Object[dd.size()];
                if (!dd.isEmpty()) {
                    boolean isdoc = dd.iterator().next() instanceof Document;
                    for (int i = 0; i < dd.size(); i++)
                        if (isdoc)
                            mmap[i] = read((Document) dd.get(i));
                        else
                            mmap[i] = dd.get(i);
                }
                map.put(e.getKey(), mmap);
            } else
                map.put(e.getKey(), e.getValue());
        }
        return map;
    }

}