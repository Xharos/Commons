package fr.xharos.database;

import fr.xharos.functional.Disposable;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * File <b>AbstractDatabase</b> located on fr.xharos.database
 * AbstractDatabase is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 01/07/2017 at 21:55
 * @since 0.0.1
 */
public abstract class AbstractDatabase<T, O> implements Disposable {

    protected final String userName, password;
    protected List<InetSocketAddress>      serverAddress;
    protected String                       databaseName;
    protected Optional<DatabaseProperties> properties;

    public AbstractDatabase(int port, String host, String userName, String password) {
        this(port, host, userName, password, null);
    }

    public AbstractDatabase(InetSocketAddress address, String userName, String password) {
        this(address, userName, password, null);
    }

    public AbstractDatabase(int port, String host, String userName, String password, String databaseName) {
        this(InetSocketAddress.createUnresolved(host, port), userName, password, databaseName);
    }

    public AbstractDatabase(InetSocketAddress address, String userName, String password, String databaseName) {
        this.serverAddress = Arrays.asList(address);
        this.userName = userName;
        this.password = password;
        this.databaseName = databaseName;
    }

    public AbstractDatabase(List<InetSocketAddress> addressList, String userName, String password, String databaseName) {
        this.serverAddress = addressList;
        this.userName = userName;
        this.password = password;
        this.databaseName = databaseName;
    }

    public boolean propertiesExist() {
        return properties != null && properties.isPresent();
    }

    public void provideProperty(DatabaseProperties connectionProperties) {
        this.properties = Optional.of(connectionProperties);
    }

    public abstract void connect() throws Exception;

    public abstract void close() throws Exception;

    public abstract T getConnection();

    protected abstract O buildOptions(DatabaseProperties properties);

    @Override
    public void dispose() throws Exception {
        close();
    }

}
