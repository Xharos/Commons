package fr.xharos.database;

/**
 * File <b>DatabasePropertiesKey</b> located on fr.xharos.database
 * DatabasePropertiesKey is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 02/07/2017 at 22:58
 * @since 0.0.2
 */
public enum DatabasePropertiesKey {
//maybe add boolean in parameter to see if we can use this properties in specific database

    MAX_CONFIG;

    public static DatabasePropertiesKey getFromString(String name) {
        for (DatabasePropertiesKey databasePropertiesKey : DatabasePropertiesKey.values()) {
            if (databasePropertiesKey.name().equals(name))
                return databasePropertiesKey;
        }
        return null;
    }

}
