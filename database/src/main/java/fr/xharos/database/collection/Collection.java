package fr.xharos.database.collection;

import fr.xharos.functional.Value;
import org.bson.Document;

/**
 * File <b>Collection</b> located on fr.xharos.database.collection
 * Collection is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 * @author Sceat {@literal <sceat@aresrpg.fr>}
 *         Created the 01/07/2017 at 22:16
 * @since 0.0.1
 */
public interface Collection<T> extends Value<T> {

    /**
     * Put the value in the collection
     *
     * @param value the value
     */
    void put(T value);

    /**
     * Update the value in the collection
     *
     * @param filter the filter to find the value
     * @param value  the new value
     */
    void update(Filter filter, T value);

    /**
     * Put or update the value in the collection
     *
     * @param filter the filter to find the value
     * @param value  the new value
     */
    void putOrUpdate(Filter filter, T value);

    /**
     * Update all the value in the collection
     *
     * @param filter the filter to find the values
     * @param value  the new value
     */
    void updateAll(Filter filter, T value);

    /**
     * Put or update all the value in the collection
     *
     * @param filter the filter to find the values
     * @param value  the new value
     */
    void putOrUpdateAll(Filter filter, T value);

    /**
     * Find the values in the collection
     *
     * @param filter the filter to use
     * @param max    the maximum of value to get
     * @return the values found or null if there was no values
     */
    T[] find(Filter filter, int max);

    /**
     * Find the values in the collection and sort them
     *
     * @param fieldname the field name
     * @param max       the maximum values to get
     * @return the founded values or null if there was no values
     */
    T[] sorted(String fieldname, int max);

    /**
     * Find the first value corresponding to the filter
     *
     * @param filter the filter
     * @return the value or null if there is no value corresponding to the filter
     */
    default T findFirst(Filter filter) {
        T[] found = find(filter, 1);
        if (found.length >= 1)
            return found[0];
        else
            return null;
    }

    /**
     * Remove the values matching the filter
     *
     * @param filter  the filter to use
     * @param removed the maximum of values to remove
     * @return the number of values removed
     */
    int remove(Filter filter, int removed);

    /**
     * Remove a value from the collection
     *
     * @param filter the filter to find the value
     * @return true if the value has bean removed
     */
    default boolean remove(Filter filter) {
        return remove(filter, 1) == 1;
    }

    /**
     * Get if the values exist in the database
     *
     * @param filter the filter to use
     * @return true if the value exist
     */
    default boolean exist(Filter filter) {
        return find(filter, 1).length != 0;
    }

    /**
     * @return the number of document in the collection
     */
    long count();

    /**
     * Get the name of this collection
     *
     * @return the id of this collection
     */
    String getId();

}
