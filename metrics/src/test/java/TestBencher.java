import fr.xharos.metrics.Bencher;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SocketChannel;

/**
 * File <b>TestBencher</b> located on PACKAGE_NAME
 * TestBencher is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 30/08/2017 at 12:50
 * @since 0.0.1
 */
public class TestBencher {

    @Test
    public void schedule() {
        Bencher.test(() -> {
            Socket socket = new Socket("127.0.0.1", 25565);
        }).ifError((exception) -> {
            System.out.println("IO error when connecting");
        }).paramCount(10).jitInit(10000).run();

        Bencher.groupTest(() -> {
            Socket socket = new Socket("127.0.0.1", 25565);
        }, () -> {
            SocketChannel socket = SocketChannel.open();
            socket.connect(new InetSocketAddress("127.0.0.1",25565));
        }).ifError((exception, testId) -> {
            System.out.println("IO error when connecting on test "+testId);
        }).paramCount(10).jitInit(10000).compareLog().run();
    }
}
