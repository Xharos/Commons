package fr.xharos.metrics;

import java.util.function.Consumer;

/**
 * File <b>Bencher</b> located on fr.xharos.metrics
 * Bencher is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 30/08/2017 at 12:45
 * @since 0.0.1
 */
public class Bencher implements Runnable {

    private final BencherRunnable     action;
    private       Consumer<Exception> errorAction;
    private       int                 count;
    private       long                maxJitTime;

    Bencher(BencherRunnable code, int count) {
        this.maxJitTime = 0L;
        this.action = code;
        this.count = count;
    }

    public static Bencher test(BencherRunnable action) {
        return new Bencher(action, 1);
    }

    public Bencher paramCount(int newCount) {
        if (newCount > 1)
            count = newCount;
        return this;
    }

    public Bencher ifError(Consumer<Exception> action) {
        this.errorAction = action;
        return this;
    }

    public Bencher jitInit(long maxTime) {
        this.maxJitTime = maxTime <= 0 ? 0 : maxTime;
        return this;
    }

    @Override
    public void run() {
        if (maxJitTime != 0) {
            long start = System.currentTimeMillis();
            for (int i = 0; i < 11000; i++) {
                if (System.currentTimeMillis() - start >= maxJitTime)
                    break;
                try {
                    action.run();
                } catch (Exception e) {
                    continue;
                }
            }
        }
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1; i++) {
            try {
                action.run();
            } catch (Exception e) {
                if (errorAction != null)
                    errorAction.accept(e);
                else {
                    e.printStackTrace();
                    break;
                }
            }
        }
        System.out.println("Execute given code " + count + " time" + (count >= 1 ? "s" : "") + " in " + (System.currentTimeMillis() - start) + " ms!");
    }

    public static interface BencherRunnable {

        void run() throws Exception;

    }

}
