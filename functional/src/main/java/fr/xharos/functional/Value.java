package fr.xharos.functional;

import java.util.StringJoiner;

/**
 * File <b>Value</b> located on fr.xharos.functional
 * Value is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 22:24
 * @since 0.1.0
 */
public interface Value<T> extends Iterable<T> {

    default void debug() {
    }

    /**
     * Transform this object to a String for debug
     *
     * @return a nice String representation of the object
     */
    default String stringify() {
        StringJoiner joiner = new StringJoiner(", ");
        for (T value : this)
            joiner.add(String.valueOf(value));
        return objectName() + "[" + joiner.toString() + "]";
    }

    /**
     * Get the name of the object's class
     *
     * @return the object name
     */
    default String objectName() {
        return getClass().getName();
    }

    /**
     * Check if this object is empty
     *
     * @return true if the object is empty
     */
    boolean isEmpty();

    static String toString(Object o) {
        if (o instanceof Value)
            return ((Value) o).stringify();
        else
            return o.toString();
    }
}
