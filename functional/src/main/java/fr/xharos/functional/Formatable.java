package fr.xharos.functional;

/**
 * File <b>Formatable</b> located on fr.xharos.functional
 * Formatable is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 14:25
 * @since 0.0.2
 */
public interface Formatable<I, O> {

    /**
     * Convert the value to the output
     *
     * @param in the input value
     * @return a representation of the input value
     */
    O adaptTo(I in);

    /**
     * Convert back this class into the value
     *
     * @return the value
     */
    I adaptFrom();

}
