package fr.xharos.test;

import fr.xharos.functional.Disposable;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * File <b>FunctionalTest</b> located on fr.xharos.test
 * FunctionalTest is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2017 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 28/06/2017 at 13:38
 * @since 0.1.0
 */
public class FunctionalTest {

    @Test
    public void HahsMapTest() {
        MapTest test = new MapTest();
        test.maps.put("key",12);
        try {
            test.dispose();
        }catch(Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue(test.maps.hashCode() == 0);
    }

    public class MapTest implements Disposable {

        private Map<String, Integer> maps = new HashMap<>();

        @Override
        public void dispose() throws Exception {
            maps.clear();
        }
    }

}
