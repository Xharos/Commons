package fr.xharos.serialization.factory;

import fr.xharos.reflection.ParametrizedClass;
import fr.xharos.serialization.FieldNamer;
import fr.xharos.serialization.adapter.Adapter;
import fr.xharos.serialization.serializer.Serializer;
import java.util.ArrayList;
import java.util.List;

/**
 * File <b>SerializationFactory</b> located on fr.xharos.serialization.factory
 * SerializationFactory is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 * @author Duarte David  {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:33
 * @since 0.0.1
 */
public interface SerializationFactory {

    FieldNamer getFieldNamer();

    void setFieldNamer(FieldNamer namer);

    List<Adapter<?, ?>> getAdapters();

    void addAdapter(Adapter<?, ?> adapter);

    void removeAdapter(Adapter<?, ?> adapter);

    default Adapter[] getAdapterChain(ParametrizedClass c) {
        Adapter last;
        ParametrizedClass clazz = c;
        List<Adapter> adapters = new ArrayList<>();
        while ((last = getAdapter(clazz)) != null) {
            adapters.add(last);
            clazz = last.getOutType();
        }
        return adapters.toArray(new Adapter[adapters.size()]);
    }

    <T> Adapter<T, ?> getAdapter(ParametrizedClass<T> clazz);

    <T> Serializer<T> createSerializer(Class<T> clazz);

    <T> Serializer<T> createOrGetSerializer(Class<T> clazz);

}
