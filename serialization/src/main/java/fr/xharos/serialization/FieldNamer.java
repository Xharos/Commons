package fr.xharos.serialization;

import java.lang.reflect.Field;

/**
 * File <b>FieldNamer</b> located on fr.xharos.serialization
 * FieldNamer is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:49
 * @since 0.0.1
 */
@FunctionalInterface
public interface FieldNamer {

    /**
     * Get the name of the field
     *
     * @param field the source field
     * @return a transformed name
     */
    String getName(Field field);
}
