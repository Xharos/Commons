package fr.xharos.serialization;

import java.io.IOException;

/**
 * File <b>SerializationContext</b> located on fr.xharos.serialization
 * SerializationContext is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:29
 * @since 0.0.1
 */
@FunctionalInterface
public interface SerializationContext {

    /**
     * Serialize this value using the provided format
     *
     * @param out    the output
     * @param value  the value to serialize
     * @param format the format to use
     * @param <T>    the handler of the value
     * @param <O>    the out
     * @throws IOException when the output writing fails
     */
    <T, O> void serialize(O out, T value, Format<?, O> format) throws IOException;
}
