package fr.xharos.serialization;

import java.io.IOException;

/**
 * File <b>Format</b> located on fr.xharos.serialization
 * Format is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:26
 * @since 0.0.1
 */
public interface Format<I, O> {

    /**
     * Called on the beginning of a serialization
     *
     * @param out the output
     * @throws IOException if an error occurred during writing
     */
    void writeBegin(O out) throws IOException;

    /**
     * Write a field value
     *
     * @param out     the output
     * @param name    the name or null if this is a solo value
     * @param type    the handler of the value
     * @param value   the value
     * @param context the context of the serialization
     * @throws IOException if an error occurred during writing
     */
    void writeValue(O out, String name, TypeEnum type, Object value, SerializationContext context) throws IOException;

    /**
     * Called before an object's writing
     *
     * @param out the output
     * @throws IOException if an error occurred during writing
     */
    void writeBeginObject(O out) throws IOException;

    /**
     * Called on separation of fields
     *
     * @param out        the output
     * @param firstField if is the first field of the object
     * @param lastField  if is the last field of the object
     * @throws IOException if an error occurred during writing
     */
    void writeFieldSeparator(O out, boolean firstField, boolean lastField) throws IOException;

    /**
     * Called after the writing of an object
     *
     * @param out the output
     * @throws IOException if an error occurred during writing
     */
    void writeEndObject(O out) throws IOException;

    /**
     * Called at the end of a serialization
     *
     * @param out the output
     * @throws IOException if an error occurred during writing
     */
    void writeEnd(O out) throws IOException;

    /**
     * Read an object from input
     *
     * @param in the input
     * @return an object of representing handler (For object an Map is returned)
     * @throws IOException if an error occurred during reading
     */
    Object read(I in) throws IOException;
}
