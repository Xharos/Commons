package fr.xharos.serialization;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * File <b>typeEnum</b> located on fr.xharos.serialization
 * typeEnum is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:28
 * @since 0.0.1
 */
public enum TypeEnum {
    //Primitive
    BOOLEAN,
    BYTE,
    SHORT,
    CHAR,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    NULL,

    //Base
    STRING,
    COLLECTION,
    MAP,

    //Arrays
    OBJECT_ARRAY(true),
    BOOLEAN_ARRAY(true),
    BYTE_ARRAY(true),
    SHORT_ARRAY(true),
    CHAR_ARRAY(true),
    INT_ARRAY(true),
    LONG_ARRAY(true),
    FLOAT_ARRAY(true),
    DOUBLE_ARRAY(true),

    //Last
    OBJECT;

    private boolean array;

    private TypeEnum() {
        this(false);
    }

    private TypeEnum(boolean array) {
        this.array = array;
    }

    /**
     * @return the array
     */
    public boolean isArray() {
        return array;
    }

    /**
     * Get the handler enum for this class
     *
     * @param clazz the class
     * @return the handler enum for the class
     */
    public static TypeEnum getType(Class<?> clazz) {
        String name = clazz.getName();
        TypeEnum type = getType(name);
        if (type != null)
            return type;
        else if (clazz.isArray()) {
            if (clazz.getComponentType().isPrimitive())
                switch (getPrimitiveType(clazz.getComponentType().getName())) {
                    case BOOLEAN:
                        return BOOLEAN_ARRAY;
                    case BYTE:
                        return BYTE_ARRAY;
                    case SHORT:
                        return SHORT_ARRAY;
                    case CHAR:
                        return CHAR_ARRAY;
                    case INT:
                        return INT_ARRAY;
                    case LONG:
                        return LONG_ARRAY;
                    case FLOAT:
                        return FLOAT_ARRAY;
                    case DOUBLE:
                        return DOUBLE_ARRAY;
                    default:
                        throw new IllegalStateException("Unreachable");
                }
            else
                return OBJECT_ARRAY;
        } else if (name.startsWith("java.util"))
            if (Collection.class.isAssignableFrom(clazz))
                return COLLECTION;
            else if (Map.class.isAssignableFrom(clazz))
                return MAP;
            else
                return OBJECT;
        else
            return OBJECT;
    }

    /**
     * Get the handler enum for this class name
     *
     * @param name the name of the class
     * @return the handler enum for the class
     */
    public static TypeEnum getType(String name) {
        switch (name) {
            case "java.lang.String":
                return STRING;
            case "java.lang.Boolean":
                return BOOLEAN;
            case "java.lang.Byte":
                return BYTE;
            case "java.lang.Short":
                return SHORT;
            case "java.lang.Character":
                return CHAR;
            case "java.lang.Integer":
                return INT;
            case "java.lang.Long":
                return LONG;
            case "java.lang.Float":
                return FLOAT;
            case "java.lang.Double":
                return DOUBLE;
            default:
                return getPrimitiveType(name);
        }
    }

    public static TypeEnum getPrimitiveType(Class clazz) {
        Objects.requireNonNull(clazz);
        TypeEnum value = getPrimitiveType(clazz.getName());
        return value == null ? OBJECT : value;
    }

    public static TypeEnum getPrimitiveType(String name) {
        switch (name) {
            case "boolean":
                return BOOLEAN;
            case "byte":
                return BYTE;
            case "short":
                return SHORT;
            case "char":
                return CHAR;
            case "int":
                return INT;
            case "long":
                return LONG;
            case "float":
                return FLOAT;
            case "double":
                return DOUBLE;
            default:
                return null;
        }
    }

    public static TypeEnum getType(Object object) {
        if (object == null)
            return NULL;
        else
            return getType(object.getClass());
    }
}
