package fr.xharos.serialization.serializer;

import fr.xharos.serialization.Format;
import java.io.IOException;
import java.util.Map;

/**
 * File <b>Serializer</b> located on fr.xharos.serialization.serializer
 * Serializer is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:34
 * @since 0.0.1
 */
public interface Serializer<T> {

    /**
     * Map the object, serialize it using the passed format and write the result to the output
     *
     * @param output output to write the result
     * @param object the object to serialize
     * @param format the format to use in serialization
     * @param <O>    the output handler
     * @throws IOException if an exception occurred
     * @throws IOException if an exception occurred
     */
    <O> void serialize(O output, T object, Format<?, O> format) throws IOException;

    /**
     * Map the object and deserialize it in the provided object using the passed format from the input
     *
     * @param input  input
     * @param object the object to write in
     * @param format the format to use in deserialization
     * @param <I>    the input handler
     * @throws IOException if an exception occurred
     */
    <I> void deserialize(I input, T object, Format<I, ?> format) throws IOException;

    /**
     * Map the object and deserialize it using the passed format from the input
     *
     * @param input  input
     * @param format the format to use in deserialization
     * @param <I>    the input handler
     * @return the mapped object
     * @throws IOException if an exception occurred
     */
    <I> T deserialize(I input, Format<I, ?> format) throws IOException;

    /**
     * Map the object passed in argument
     *
     * @param values the values to map
     * @return the mapped object
     * @throws IOException if an exception occured
     */
    T deserialize(Map<String, Object> values) throws IOException;

    /**
     * Map the object passed in argument
     *
     * @param values the values to map
     * @param object the mapped object
     */
    void deserialize(Map<String, Object> values, T object) throws IOException;
}