package fr.xharos.serialization.adapter;

import fr.xharos.reflection.ParametrizedClass;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * File <b>ListAdapter</b> located on fr.xharos.serialization.adapter
 * ListAdapter is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David  {@literal <deltaduartedavid@gmail.com>}
 *         Created the 02/07/2017 at 00:13
 * @since 0.0.1
 */
public class ListAdapter implements Adapter<List, Object[]> {

    public static final Adapter<List, Object[]> INSTANCE = new ListAdapter();

    public static final ParametrizedClass<Object[]> OUT = new ParametrizedClass<Object[]>() {
    };
    public static final ParametrizedClass<List>     IN  = new ParametrizedClass<List>() {
    };

    private ListAdapter() {
    }

    @Override
    public Object[] adaptTo(List in) {
        return in.toArray(new Object[in.size()]);
    }

    @Override
    public List adaptFrom(Object[] out) {
        return new ArrayList(Arrays.asList(out));
    }

    @Override
    public ParametrizedClass<List> getInType() {
        return IN;
    }

    @Override
    public ParametrizedClass<Object[]> getOutType() {
        return OUT;
    }
}
