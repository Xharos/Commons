package fr.xharos.serialization.impl.unsafe;

import fr.xharos.reflection.ParametrizedClass;
import fr.xharos.serialization.FieldNamer;
import fr.xharos.serialization.adapter.Adapter;
import fr.xharos.serialization.annotations.SerializedNameFieldNamer;
import fr.xharos.serialization.factory.SerializationFactory;
import fr.xharos.serialization.serializer.Serializer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * File <b>UnsafeSerializationFactory</b> located on fr.xharos.serialization.impl.unsafe
 * UnsafeSerializationFactory is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Duarte David {@literal <deltaduartedavid@gmail.com>}
 *         Created the 01/07/2017 at 23:43
 * @since 0.0.1
 */
public class UnsafeSerializationFactory implements SerializationFactory {

    private final List<Adapter<?, ?>>          adapters;
    private final Map<Class<?>, Serializer<?>> cache;
    private       FieldNamer                   namer;

    public UnsafeSerializationFactory(List<Adapter<?, ?>> adapters) {
        this.adapters = new ArrayList<>(adapters);
        this.cache = new HashMap<>();
        this.namer = new SerializedNameFieldNamer();
    }

    public UnsafeSerializationFactory() {
        this.cache = new HashMap<>();
        this.namer = new SerializedNameFieldNamer();
        this.adapters = new ArrayList<>();
    }

    @Override
    public FieldNamer getFieldNamer() {
        return namer;
    }

    @Override
    public void setFieldNamer(FieldNamer namer) {
        this.namer = namer;
    }

    @Override
    public List<Adapter<?, ?>> getAdapters() {
        return Collections.unmodifiableList(adapters);
    }

    @Override
    public void addAdapter(Adapter<?, ?> adapter) {
        if (adapters.add(adapter))
            cache.clear();
    }

    @Override
    public void removeAdapter(Adapter<?, ?> adapter) {
        if (adapters.remove(adapter))
            cache.clear();
    }

    @Override
    public <T> Adapter<T, ?> getAdapter(ParametrizedClass<T> clazz) {
        if (adapters.isEmpty())
            return null;
        for (@SuppressWarnings("rawtypes")
                Adapter adapter : adapters)
            if (clazz.equals(adapter.getInType()))
                return adapter;
        return null;
    }

    @Override
    public <T> Serializer<T> createSerializer(Class<T> clazz) {
        Serializer<T> s = createSerializerInstance(clazz);
        cache.put(clazz, s);
        return s;
    }

    @Override
    public <T> Serializer<T> createOrGetSerializer(Class<T> clazz) {
        Serializer<T> s = (Serializer<T>) cache.get(clazz);
        if (s != null)
            return s;
        else
            return createSerializer(clazz);
    }

    private <T> Serializer<T> createSerializerInstance(Class<T> clazz) {
        return  new UnsafeSerializer<>(clazz, this);
    }
}
