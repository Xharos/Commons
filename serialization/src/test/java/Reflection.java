import java.lang.reflect.Field;
import org.junit.Assert;
import org.junit.Test;

/**
 * File <b>Reflection</b> located on PACKAGE_NAME
 * Reflection is a part of Xharos - Commons.
 * <p>
 * Copyright (c) 2016 Xharos and contributors.
 * <p>
 * Xharos - Commons is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details (COPYING).
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * AssetsBuilder is a free software: See the GNU Lesser General
 * Public License for more details (COPYING file)
 *
 * @author Xharos
 *         Created the 03/07/2017 at 14:07
 * @since 0.0.2
 */
public class Reflection {

    @Test
    public void testEnum() {
        Foo instance = new Foo();
        for (Field field : instance.getClass().getDeclaredFields()) {
            System.out.println("field = " + field.getName());
            try {
                field.setAccessible(true);
                field.set(instance, Enum.valueOf((Class<Enum>) field.getType(), "TEST1"));
                field.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Assert.assertTrue(instance.basicEnum == BasicEnum.TEST1);
    }

}
